# Bike parts list

This repository contains the list of bike parts used to print the list at the bike shed door and the signs in the bike shed shelf.
This data is supposed to be used with the script in the repository
[bikepartsigns-code](https://gitlab.com/kanthaus/bikepartsigns-code)

## Updating the digital version

The bike parts list can be found in the repository [bikepartsigns-code](https://gitlab.com/kanthaus/bikepartsigns-data).

1. Update the file `parts.json` with the information collected handwritten on the list.
   (Both alternative names as well as new parts/ boxes.)
   * Learn the syntax of `parts.json` from the existing entries and from `parts.json` in the [source repository](https://gitlab.com/kanthaus/bikepartsigns-code)
   * Collect all alternative names you and others can think of and the correct german and english terms.
   * Find a suitable picture (best: white background) on the web or take one yourself.
   * Put this image and a rotated version into the two images directories.
   * Link it by putting the correct name into the `parts.json` file.
   * Make sure the key `"printed"` is not in the parts that you changed:
   the script will only print signs for parts where `"printed"` has not
   a `true` value.
2. Get the [source repository](https://gitlab.com/kanthaus/bikepartsigns-code.git).
3. In order to have the necessary python packages available you can use `conda`:
   ```
    cd <source repository>
    conda env create -f environment.yml
    conda activate bikeparts
   ```
   Or install the packages that are listed in the file `environment.yml`.
   At the moment you need a LaTeX installation to create the long list.
4. Run the script `generate_labels.py` while being in the directory of the bike parts list.
  * Call the script `generate_labels.py` with the option `-h` to get further usage instructions if interested.
  * Call the script `generate_labels.py -l` for creating the long list (pdf-file `out/parts.pdf`) and the signs (`out/parts.html`). (! LaTeX installation needed!)
  * Check `out/parts.log` for errors and warnings.
5. Print the long list `out/parts.pdf` and the new signs `out/signs.html`. For the `signs.html` I had less trouble with the printer by first printing the pages to pdf with firefox and then printing the pdf with a pdf viewer on the actual printer.
6. Run the script `datacleanup.py -o parts.json printed` (which is also in the [source repository](https://gitlab.com/kanthaus/bikepartsigns-code)). That makes sure that next time only signs are printed that actually have changed.
7. Commit and upload your changes (new images, changes in `parts.json`).


<!-- this file should be converted to pdf and then printed and put on the door of the bike shed -->

---
geometry: margin=1.5cm
lang: en
---
<!-- Compile with
`pandoc explanation.md -o out/explanation.pdf`
See pandoc.org for Download and information -->

# Bike shed: You found bike parts!

## Finding bike parts

Look in the long list to you find your part.
All things are sorted alphabetically.
I tried to list all names you could give to things with a link to the corresposponding “correct” name and the location.
The naming scheme is
```
<shelf part><level>-<box number><in-box-numbering><height>
```
For example: Axle nuts are in L7-2e: in the left shelf part, on level 7, in the second box from the left as the fifth item.

The more detailed description of the naming scheme goes as follows:
The shelf parts are

* A = top shelf at the deep end of the shed
* B = top shelf in front of A
* Side = hanging on the left side of the shelf
* L = left third of the shelf
* M = middle third of the shelf
* R = right third of the shelf

and the levels are counted from the floor to the top, approximately one level per 12cm. Not all levels correspond to shelf boards.
The boxes within the shelf boards are numbered from left to right.

Within boxes we count with letters. If several (sub)boxes are on top of each other (`<height>`) we count with roman numerals.

If you found your part but did not find it via the list, add the term that you would have needed to find it. There is space at the end of the list.

## Storing bike parts

If you have a bike part in your hand and do not need it, try to find the correct box in the shelf.
Use the pictures on the signs and the list. Try to be creative in finding the correct term.
If you figured out where to put it but did not find it on the list, add the term that you would have needed to find it. There is space at the end of the list.

If you are not sure if you found the correct box, **do not put it somewhere random** but into the to-sort-basket in `M1`.

If you are pretty sure that there is no suitable box, you can create a new box. Put it somewhere where it roughly belongs
(where similar things are), add it onto the list together with the location and put a (handwritten) sign on it.

## Updating the digital version
<!-- probably just reference to some instructions online?! -->

Check out the the repository https://gitlab.com/kanthaus/bikepartsigns-data.
Follow the included README.

<!-- # Fahrradlager -->
<!--  -->
<!-- Deutsche Übersetzung folgt, sobald englische Version halbwegs verständlich ist. -->
